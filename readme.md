# Binary File Access Library
## Argochamber Interactive 2016

This library takes care of encoding and decoding a file written in binary for you.

Just use the annotatons over public fields in a class to determine the schema.

### Examples

Example of __Schema definition__
```java
public class TestType {
    
    @Field( id = 0, len = 1 )
    public Byte fieldA;
    
    @Field( id = 1, len = 2 )
    public Character fieldB;
    
    @Field( id = 2, len = 32 )
    public String fieldStr;
    
}
```

Example of binary data __encoding__
```java
BinFile bin = new BinFile(TestType.class);

TestType t1 = new TestType();
t1.fieldA = 5;
t1.fieldB = 'a';
t1.fieldStr = "string test! yay!";

bin.insertRow(Row.asRow(t1));

bin.storeTo(new File("test.bin"));
```

Example of binary data __decoding__
```java
int rode = bin.readFrom(new File("test.bin"), true);

TestType test = (TestType)Row.asObject(bin.getRow(1), TestType.class);
```
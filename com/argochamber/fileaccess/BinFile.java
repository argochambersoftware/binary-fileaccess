/*
 * Argochamber Interactive 2016
 * Binary transcription library.
 * 
 * This library is used to store primitive sets of data, including Strings to binary files.
 * 
 * By SigmaSoldi3R
 */
package com.argochamber.fileaccess;

import com.argochamber.fileaccess.data.Dataset;
import com.argochamber.fileaccess.data.Row;
import com.argochamber.fileaccess.data.Field;
import com.argochamber.fileaccess.data.FieldDescriptor;
import com.argochamber.fileaccess.io.ByteDataInput;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <h1>Binary File Factory</h1>
 * <p>
 *  This class will take care of encoding and decoding data from a <strong>given
 *  type of object</strong>, this object will <i>define the structure of data</i>.
 * </p>
 * <p>
 *  The data is stored in <b>rows</b>, and those rows have <b>columns</b>.
 *  Where each row can be defined with an <i>object</i>, each colum will be defined
 *  by the class that acts as mapping class.
 * </p>
 * <p>
 *  It is important to provide to the <b>Bin File</b> a class object that defines
 *  the schema!
 * </p>
 * <hr>
 * <p>
 *  The <i>class</i> must have the following:
 *  <ul>
 *      <li>Public access fields</li>
 *      <li>@Field annotation at the very top of each <b>field</b></li>
 *  </ul>
 *  See the field annotation to see how to.
 * </p>
 * @see com.argochamber.fileaccess.data.Field
 * @author Pablo
 */
public class BinFile {

    /**
     * The rows of data, contains k-v pairs.
     * @see com.argochamber.fileaccess.data.Row
     */
    private Dataset rows;

    /**
     * Stores the schema of the given class type.
     * @see com.argochamber.fileaccess.data.FieldDescriptor
     */
    private ArrayList< FieldDescriptor> schema;

    /**
     * The count of bytes for each row.
     */
    private int bytesPerRow;

    /**
     * Builds up the binary file type.
     *
     * @param t This is the descriptor class, that must have the @Field annotations!
     */
    public BinFile(Class t) {
        //Scan the type and build the schema
        this.schema = new ArrayList<>();
        this.rows = new Dataset();
        this.bytesPerRow = 0;

        //Initialize array data
        for (java.lang.reflect.Field declaredField : t.getDeclaredFields()) {
            this.schema.add(new FieldDescriptor("null", 0, Object.class));
        }

        for (java.lang.reflect.Field f : t.getDeclaredFields()) {
            Annotation an = f.getDeclaredAnnotation(Field.class);
            if (an != null) {
                Field field = ((Field) an);

                //Store the schema of the class.
                schema.set(field.id(), new FieldDescriptor(f.getName(), field.len(), f.getType()));

                //Finally count bytes.
                bytesPerRow += field.len();
            }
        }

    }

    /**
     * Inserts a row to the data set.
     *
     * @param row The row data.
     */
    public final void insertRow(Row row) {
        this.rows.add(row);
    }

    /**
     * Gets the specified row in the set.
     *
     * @param id Primary key for the row
     * @return
     */
    public final Row getRow(int id) {
        return this.rows.get(id);
    }

    /**
     * Reads the file, returns the row count.
     *
     * @param file Input file.
     * @param clear If true clears the rows of the bin file, else appends.
     * @return The amount of rows rode.
     */
    public int readFrom(java.io.File file, boolean clear) {
        if (clear) {
            this.rows.clear();
        }
        try {
            ByteDataInput input = new ByteDataInput(new FileInputStream(file));
            int rode = 0;
            byte[] data = input.toByteArray();

            //For each row:
            for (int i = 0; i < data.length / this.bytesPerRow; i++) {
                Row row = new Row();
                //Elements in the schema
                //System.out.println("============= ITER "+i+" ("+(i*this.bytesPerRow)+"/"+data.length+")============");
                int j = 0;
                for (FieldDescriptor desc : schema) {
                    
                    byte[] raw = Arrays.copyOfRange(data, i * this.bytesPerRow + j,i * this.bytesPerRow + desc.bytes+j);
                    //System.out.println("Len bytes; "+raw.length+" ps = "+(i*this.bytesPerRow)+" offs: "+j+" sLen = "+desc.bytes);
                    //Data extraction
                    ByteBuffer buff = ByteBuffer.wrap(raw);
                    Object obj = null;

                    if (desc.type.equals(Integer.class)) {
                        obj = buff.getInt();
                    } else if (desc.type.equals(Byte.class)) {
                        obj = buff.get();
                    } else if (desc.type.equals(Short.class)) {
                        obj = buff.getShort();
                    } else if (desc.type.equals(Long.class)) {
                        obj = buff.getLong();
                    } else if (desc.type.equals(Float.class)) {
                        obj = buff.getFloat();
                    } else if (desc.type.equals(Double.class)) {
                        obj = buff.getDouble();
                    } else if (desc.type.equals(Character.class)){
                        obj = buff.getChar();
                    } else if (desc.type.equals(String.class)) {
                        try {
                            obj = new String(raw, "UTF-8");
                        } catch (UnsupportedEncodingException ex) {
                            obj = new String(raw);
                        }
                    } else {
                        System.err.println("NOT SUPPORTED TYPE '" + desc.type.toString() + "'");
                    }
                    
                    //System.out.println("Done! "+obj.toString());

                    //Store extracted data.
                    row.put(desc.name, obj);
                    j += desc.bytes;
                    rode += desc.bytes;
                }
                this.rows.add(row);
            }
            
            try {
                //Now close
                input.close();
            } catch (IOException ex) {
                Logger.getLogger(BinFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return rode;
        } catch (FileNotFoundException ex) {
            System.err.println("Could not read the file \"".concat(file.getName()).concat("\""));
            System.err.println("Cause: ".concat(ex.getMessage()));
            return -1;
        }
    }

    /**
     * Stores the data to the given file.
     *
     * @param file
     * @return Could the file get stored?
     */
    public boolean storeTo(java.io.File file) {
        try {
            FileOutputStream fstream = new FileOutputStream(file);

            for (Row r : this.rows) {
                for (FieldDescriptor desc : schema) {
                    for (Entry<String, Object> entry : r.entrySet()) {
                        String key = entry.getKey();
                        Object value = entry.getValue();

                        if (desc.name.equals(key)) {
                            ByteBuffer buff = ByteBuffer.allocate(desc.bytes);
                            
                            //System.out.println("Incoming data: "+desc.name+": "+desc.type+" byte/s = "+desc.bytes);

                            if (desc.type.equals(Integer.class)) {
                                buff.putInt(Integer.valueOf(value.toString()));
                            } else if (desc.type.equals(Byte.class)) {
                                buff.put(Byte.valueOf(value.toString()));
                            } else if (desc.type.equals(Short.class)) {
                                buff.putShort(Short.valueOf(value.toString()));
                            } else if (desc.type.equals(Long.class)) {
                                buff.putLong(Long.valueOf(value.toString()));
                            } else if (desc.type.equals(Float.class)) {
                                buff.putFloat(Float.valueOf(value.toString()));
                            } else if (desc.type.equals(Double.class)) {
                                buff.putDouble(Double.valueOf(value.toString()));
                            } else if (desc.type.equals(Character.class)){
                                buff.putChar(value.toString().charAt(0));
                            } else if (desc.type.equals(String.class)) {
                                int len = (desc.bytes / Character.BYTES);
                                String res = String.format("%1."+len+"s", (String) value);
                                buff.put(res.getBytes("UTF-8"));
                            } else {
                                System.err.println("NOT SUPPORTED TYPE '" + desc.type.toString() + "'");
                            }

                            //System.out.println("Wrote " + desc.bytes + " bytes of binary data, type: " + desc.type.toString());
                            fstream.write(buff.array());
                        }

                    }
                }

            }
            
            //Now close
            fstream.close();

            return true;
        } catch (IOException io) {
            System.err.println("Could not store the file \"".concat(file.getName()).concat("\""));
            System.err.println("Cause: ".concat(io.getMessage()));
            return false;
        }
    }

    /**
     * Counts the rows.
     * @return 
     */
    public int getRowCount() {
        return this.rows.size();
    }

}

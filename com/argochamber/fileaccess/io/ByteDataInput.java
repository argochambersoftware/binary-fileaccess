/*
 * Argochamber Interactive 2016
 * Binary transcription library.
 * 
 * This library is used to store primitive sets of data, including Strings to binary files.
 * 
 * By SigmaSoldi3R
 */
package com.argochamber.fileaccess.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * <h1>Byte Data Reader</h1>
 * <hr>
 * <p>
 *  This is an input stream reader type, you give any <b>input stream</b> type,
 *  and this will take care of reading the byte data all at once.
 * </p>
 * @author Pablo
 */
public class ByteDataInput extends InputStreamReader {
    
    /**
     * The data.
     */
    private byte[] data;
    
    /**
     * The input stream.
     */
    private InputStream in;
    
    /**
     * Needed a input stream, will load to byte data.
     * @param in The input stream to read.
     */
    public ByteDataInput(InputStream in) {
        super(in);
        this.in = in;
    }
    
    /**
     * Reads the file to the internal array, returns the length of bytes.
     * @return  the length of bytes readed
     * @throws java.io.IOException 
     */
    @Override
    public int read() throws IOException{
        ArrayList<Byte> bytes = new ArrayList<>();
        int last;
        do {
            last = this.in.read();
            if (last >= 0){
                bytes.add((byte)last);
            }
        } while( last != -1 );
        
        //Once data is read, dump to object's field.
        this.data = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++){
            this.data[i] = bytes.get(i);
        }
        return bytes.size();
    }
    
    /**
     * Gets the data.
     * @return if already readed, the data readed.
     */
    public byte[] getData(){
        return this.data;
    }
    
    /**
     * Tries to read the data, returns empty array if something goeas wrong.
     * @return the byte data of the file.
     */
    public byte[] toByteArray(){
        try {
            this.read();
            return this.getData();
        } catch(IOException ex){
            System.err.println("Could not read the data!!");
            return new byte[0];
        }
    }
    
    
}

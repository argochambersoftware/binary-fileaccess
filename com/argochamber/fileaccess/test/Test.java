/*
 * Argochamber Interactive 2016
 * Binary transcription library.
 * 
 * This library is used to store primitive sets of data, including Strings to binary files.
 * 
 * By SigmaSoldi3R
 */
package com.argochamber.fileaccess.test;

import com.argochamber.fileaccess.BinFile;
import com.argochamber.fileaccess.data.Row;
import com.argochamber.fileaccess.io.ByteDataInput;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;

/**
 * Test mainfile
 * @author Pablo
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        
        System.out.println("Writting binary data...");
        
        BinFile bin = new BinFile(TestType.class);
        
        TestType t1 = new TestType();
        t1.fieldA = 5;
        t1.fieldB = 'a';
        t1.fieldStr = "string test! yay!";
        
        bin.insertRow(Row.asRow(t1));
        
        TestType t2 = new TestType();
        t2.fieldA = 15;
        t2.fieldB = 'o';
        t2.fieldStr = "Lel this fialdus";
        
        bin.insertRow(Row.asRow(t2));
        
        boolean could = bin.storeTo(new File("test.bin"));
        
        System.out.println("Stored successfully? "+could);
        
        System.out.println("Data sanity check:");
        
        ByteDataInput inp = new ByteDataInput( new FileInputStream(new File("test.bin")));
        
        String msg = Arrays.toString(inp.toByteArray());
        System.out.println(msg);
        
        System.out.println("--- REVERSE JOB: READ FILE ---");
        
        int read = bin.readFrom(new File("test.bin"), true);
        
        TestType t3 = (TestType)Row.asObject(bin.getRow(0), TestType.class);
        
        System.out.println("Read "+read+" byte/s");
        
        System.out.println(t3.toString());
        
    }
    
}

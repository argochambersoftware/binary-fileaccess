/*
 * Argochamber Interactive 2016
 * Binary transcription library.
 * 
 * This library is used to store primitive sets of data, including Strings to binary files.
 * 
 * By SigmaSoldi3R
 */
package com.argochamber.fileaccess.test;

import com.argochamber.fileaccess.data.Field;

/**
 * Test purposes.
 * @author Pablo
 */
public class TestType {
    
    @Field( id = 0, len = 1 )
    public Byte fieldA;
    
    @Field( id = 1, len = 2 )
    public Character fieldB;
    
    @Field( id = 2, len = 32 )
    public String fieldStr;

    @Override
    public String toString() {
        return "TestType{" + "fieldA=" + fieldA + ", fieldB=" + fieldB + ", fieldStr=" + fieldStr + '}';
    }
    
    
    
}

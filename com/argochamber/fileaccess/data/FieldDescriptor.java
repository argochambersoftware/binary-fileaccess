/*
 * Argochamber Interactive 2016
 * Binary transcription library.
 * 
 * This library is used to store primitive sets of data, including Strings to binary files.
 * 
 * By SigmaSoldi3R
 */
package com.argochamber.fileaccess.data;

/**
 * <h1>Field Description Keypair</h1>
 * <hr>
 * <p>
 *  This is contains the <b>name, data length, and class type</b> information
 *  about a field of a schema.
 * </p>
 * @author Pablo
 */
public class FieldDescriptor {
    
    /**
     * Buld a simple field descriptor.
     * @param name Field's name
     * @param bytes The length in bytes
     * @param type The class type.
     */
    public FieldDescriptor(String name, int bytes, Class type) {
        this.name = name;
        this.bytes = bytes;
        this.type = type;
    }

    public String name;
    public int bytes;
    public Class type;

    @Override
    public String toString() {
        return "FieldDescriptor{" + "name=" + name + ", bytes=" + bytes + ", type=" + type + '}';
    }
    
}

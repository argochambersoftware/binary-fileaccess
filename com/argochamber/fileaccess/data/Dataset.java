/*
 * Argochamber Interactive 2016
 * Binary transcription library.
 * 
 * This library is used to store primitive sets of data, including Strings to binary files.
 * 
 * By SigmaSoldi3R
 */
package com.argochamber.fileaccess.data;

import java.util.ArrayList;

/**
 * <h1>Array List of rows.</h1>
 * <p>
 *  As not being "generic type" but wanted the List interface funcionalities,
 *  we use a non-generic arraylist type.
 * </p>
 * @author Pablo
 */
public class Dataset extends ArrayList< Row > {}

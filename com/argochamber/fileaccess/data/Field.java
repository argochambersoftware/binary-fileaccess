/*
 * Argochamber Interactive 2016
 * Binary transcription library.
 * 
 * This library is used to store primitive sets of data, including Strings to binary files.
 * 
 * By SigmaSoldi3R
 */
package com.argochamber.fileaccess.data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * <h1>Field Descriptor</h1>
 * <hr>
 * <p>
 *  This tells the <b>BinFile</b> class how to take the information schema from
 *  a given class.
 * </p>
 * <p>
 *  To use this, the class must have this annotation at the very top, where:
 *  <ul>
 *      <li>"id" is the order of storage; Must start from 0 and increment.</li>
 *      <li>"len" is the lenght of the field's data, in <b>bytes</b></li>
 *  </ul>
 * </p>
 * <h3>Example:</h3>
 * <pre>
 * {@code
 * (at symbol)Field( id = 0, len = 4 )
 * public int fieldOne;
 * }
 * </pre>
 * @see com.argochamber.fileaccess.BinFile
 * @see com.argochamber.fileaccess.test.TestType
 * @author Pablo
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Field {
    
    /**
     * The order of the field.
     * @return 
     */
    int id();
    
    /**
     * The length of the field
     * Defaults to 4 bytes.
     * @return 
     */
    int len() default 4;
    
}

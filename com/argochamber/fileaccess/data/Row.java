/*
 * Argochamber Interactive 2016
 * Binary transcription library.
 * 
 * This library is used to store primitive sets of data, including Strings to binary files.
 * 
 * By SigmaSoldi3R
 */
package com.argochamber.fileaccess.data;

import java.util.HashMap;

/**
 * <h1>Data Row</h1>
 * <hr>
 * <p>
 *  This contains the information of each cell of the row.
 * </p>
 * <p>
 *  Columns are given in string, object is the data.
 * </p>
 * @see java.util.HashMap
 * @author Pablo
 */
public class Row extends HashMap< String, Object > {
    
    /**
     * Casts the object to a row.
     * @param o
     * @return 
     */
    public static Row asRow(Object o){
        Row row = new Row();
        
        for (java.lang.reflect.Field f : o.getClass().getDeclaredFields()){
            try {
                row.put(f.getName(), f.get(o));
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                System.err.println("Could not get object \""+o.toString()+"\" as a row.");
                System.err.println(ex.toString());
            }
        }
        
        return row;
    }
    
    /**
     * Gets a row as an instance of a new object.
     * @param r
     * @param t
     * @return 
     */
    public static Object asObject(Row r, Class t){
        try {
            Object o = t.newInstance();
            
            for (java.lang.reflect.Field f : o.getClass().getFields()){
                Object val = r.get(f.getName());
                o.getClass().getDeclaredField(f.getName()).set(o, val);
            }
            
            return o;
        } catch (InstantiationException | IllegalAccessException | SecurityException | NoSuchFieldException | IllegalArgumentException ex) {
            System.err.println("Could not get Object from row");
            System.err.println(ex.toString());
            return null;
        }
    }
    
}
